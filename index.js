/**
 * @format
 */
import 'react-native-gesture-handler';
import {AppRegistry} from 'react-native';
import {name as appName} from './app.json';
import RootNavigation from "./src/navigation/RootNavigation";
import { createAppContainer } from 'react-navigation';
import App from "./App";

AppRegistry.registerComponent(appName, () => App);
