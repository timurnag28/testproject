import {action, observable} from 'mobx';
import {
  IApiResponse,
  ICity,
  IGetInfoUserResponse,
  IReviewsRating,
  IUser,
} from '../share/interfaces';
import API from '../api';
import {Alert} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';

class UserStore {
  @observable user: IUser | undefined;
  @observable userCities: ICity[] = [];
  @observable reviewsRating: IReviewsRating | undefined;
  @observable isLoading: boolean = false;

  @action.bound
  setParams = (
    user: IUser,
    userCities: ICity[],
    reviewsRating: IReviewsRating,
  ) => {
    this.user = user;
    this.userCities = userCities;
    this.reviewsRating = reviewsRating;
    this.saveUserInfoToLocal();
  };

  @action
  initUserInfo = async () => {
    await this.getUserInfoFromLocal();
    this.getUserInfo();
  };

  @action
  getUserInfo = () => {
    this.isLoading = true;
    API.user
      .getUserInfo()
      .then((response: IApiResponse<IGetInfoUserResponse>) => {
        console.log({getUser: response});
        if (response.success) {
          const {userCities, reviewsRating, ...user} = response.data;
          this.setParams(user, userCities, reviewsRating);
          this.isLoading = false;
        } else {
          throw new Error(response.message);
        }
      })
      .catch((error: Error) => {
        this.isLoading = false;
        error.message = error.message || 'Не удалось получить данные';
        Alert.alert('', error.message);
      });
  };

  @action
  deleteCity = (selectedIndex: number) => {
    let cityIds: number[] = [];
    this.userCities.map((c, index) => {
      if (selectedIndex != index) {
        cityIds.push(c.id);
      }
    });
    this.updateUserCities(cityIds);
  };

  @action
  addCity = (city: ICity) => {
    let cityIds: number[] = this.userCities.map(c => c.id);
    cityIds.push(city.id);
    this.updateUserCities(cityIds);
  };

  @action
  updateUserCities = (cityIds: number[]) => {
    this.isLoading = true;
    API.user
      .updateUser({city_ids: cityIds})
      .then((response: IApiResponse<IGetInfoUserResponse>) => {
        console.log({updateUserCities: response});
        if (response.success) {
          this.userCities = response.data.userCities;
          this.isLoading = false;
        } else {
          throw new Error(response.message);
        }
      })
      .catch((error: Error) => {
        this.isLoading = false;
        error.message = error.message || 'Не удалось получить данные';
        Alert.alert('', error.message);
      });
  };

  // save user info in local storage for offline work
  saveUserInfoToLocal = () => {
    return Promise.all([
      AsyncStorage.setItem('user', JSON.stringify(this.user)),
      AsyncStorage.setItem('userCities', JSON.stringify(this.userCities)),
      AsyncStorage.setItem('reviewsRating', JSON.stringify(this.reviewsRating)),
    ]);
  };

  // load user info from local storage
  getUserInfoFromLocal = () => {
    return Promise.all([
      AsyncStorage.getItem('user'),
      AsyncStorage.getItem('userCities'),
      AsyncStorage.getItem('reviewsRating'),
    ]).then(result => {
      if (result[0]) {
        this.user = JSON.parse(result[0]);
      }
      if (result[1]) {
        this.userCities = JSON.parse(result[1]);
      }
      if (result[2]) {
        this.reviewsRating = JSON.parse(result[2]);
      }
    });
  };
}

const userStore = new UserStore();
export default userStore;
