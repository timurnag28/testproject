import {action, observable} from 'mobx';
import {IApiResponse, ICity} from '../share/interfaces';
import API from '../api';
import {Alert} from 'react-native';

class GeoStore {
  allCities: ICity[] = [];
  @observable cities: ICity[] = [];
  @observable isLoading: boolean = false;

  @observable searchText: string = '';

  @action
  getCities = () => {
    if (!this.cities.length) {
      this.isLoading = true;
      API.geo
        .getCities()
        .then((response: IApiResponse<ICity[]>) => {
          if (response.success) {
            this.allCities = response.data;
            this.cities = response.data;
            this.isLoading = false;
          } else {
            throw new Error(response.message);
          }
        })
        .catch((error: Error) => {
          this.isLoading = false;
          error.message = error.message || 'Не удалось получить данные';
          Alert.alert('', error.message);
        });
    }
  };

  @action
  setSearchText = (text: string) => {
    this.searchText = text;
    this.cities = this.allCities.filter(c =>
      c.name.toLocaleLowerCase().includes(text.toLocaleLowerCase()),
    );
  };
}

const geoStore = new GeoStore();
export default geoStore;
