import {action, observable} from 'mobx';
import AsyncStorage from '@react-native-community/async-storage';
import API from '../api';
import {IApiResponse, IConfirmResponse} from '../share/interfaces';
import {Alert} from 'react-native';
import store from './index';

class AuthStore {
  @observable phone: string = '+71111111111';
  @observable code: string = '7777';

  accessToken: string = '';

  @action
  auth = () => {
    return API.auth
      .auth(this.phone)
      .then((response: IApiResponse<any>) => {
        console.log({authResponse: response});
        if (response.success) {
          return response;
        } else {
          throw new Error(response.message);
        }
      })
      .catch((error: Error) => {
        error.message = error.message || 'Не удалось получить данные';
        Alert.alert('', error.message);
        throw error;
      });
  };

  @action
  confirm = () => {
    return API.auth
      .confirmCode({phone: this.phone, sms_code: this.code})
      .then(async (response: IApiResponse<IConfirmResponse>) => {
        console.log({confirmCodeResponse: response});
        if (response.success) {
          const {userCities, reviewsRating, ...user} = response.data.user;
          store.userStore.setParams(user, userCities, reviewsRating);
          return this.setLocalToken(response.data.accessToken);
        } else {
          throw new Error(response.message);
        }
      })
      .catch((error: Error) => {
        error.message = error.message || 'Не удалось получить данные';
        Alert.alert('', error.message);
        throw new Error();
      });
  };

  // save in local async storage token
  setLocalToken = (token: string) => {
    this.accessToken = token;
    return AsyncStorage.setItem('token', token);
  };

  // get from local async storage token
  getLocalToken = () => {
    if (!this.accessToken) {
      return AsyncStorage.getItem('token').then(val => {
        if (val) {
          this.accessToken = val;
        }
        return this.accessToken;
      });
    }
    return Promise.resolve(this.accessToken);
  };

  @action
  setPhone = (phone: string) => {
    this.phone = phone;
  };

  @action
  setCode = (phone: string) => {
    this.code = phone;
  };
}

const authStore = new AuthStore();
export default authStore;
