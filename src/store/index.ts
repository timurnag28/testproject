import authStore from './AuthStore';
import userStore from './UserStore';
import geoStore from './GeoStore';

const store = {
  authStore,
  userStore,
  geoStore,
};

export default store;
