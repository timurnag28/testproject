import {NavigationScreenProp, NavigationState} from 'react-navigation';

export interface IApiResponse<T> {
  success: boolean;
  message: string;
  data: T;
  errors: string | string[] | null;
}

export interface IConfirmCodeBody {
  phone: string;
  sms_code: string;
}

export interface IUserUpdateBody {
  city_ids: number[];
}

export interface NavigationProps {
  navigation: NavigationScreenProp<NavigationState>;
}

export interface IConfirmResponse {
  user: IGetInfoUserResponse;
  accessToken: string;
}

export interface IGetInfoUserResponse extends IUser {
  userCities: ICity[];
  reviewsRating: IReviewsRating;
}

export interface IUser {
  id: number;
  phone: string;
  email: string;
  full_name: string;
  activity_type: string;
  displayName: string;
  avatarUrl: string;
  is_verified: number;
  statusLabel: string;
  registered_at: string;
}

export interface ICity {
  id: number;
  region_id: number;
  name: string;
}

export interface IReviewsRating {
  rating: number;
  count: number;
}
