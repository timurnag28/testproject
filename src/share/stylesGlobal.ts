import {StyleSheet} from 'react-native';
import {BLUE_COLOR} from "./colors";

const stylesGlobal = StyleSheet.create({
  container: {
    flex: 1,
  },
  center: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  bottomBorder: {
    borderBottomColor: 'grey',
    borderBottomWidth: 1,
  },
  actionButton: {
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 8,
    padding: 16,
    backgroundColor: BLUE_COLOR,
  },
  actionText: {
    color: '#fff',
    fontSize: 16,
  }
});

export default stylesGlobal;
