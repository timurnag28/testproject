import {StyleProp, Text, TouchableOpacity, ViewStyle} from 'react-native';
import React from 'react';
import stylesGlobal from "../stylesGlobal";

export const ActionButton = ({onPress, text, style}: {onPress: () => void; text: string; style?: StyleProp<ViewStyle>}) => {
  return (
    <TouchableOpacity onPress={onPress} style={[stylesGlobal.actionButton, style]}>
      <Text style={stylesGlobal.actionText}>{text}</Text>
    </TouchableOpacity>
  );
};
