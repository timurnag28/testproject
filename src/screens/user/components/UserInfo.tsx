import React, {PureComponent} from 'react';
import {View} from 'react-native';
import {UserInfoTop} from './UserInfoTop';
import {UserInfoBottom} from './UserInfoBottom';
import {IUser} from '../../../share/interfaces';

interface UserInfoProps {
  user: IUser;
}

export default class UserInfo extends PureComponent<UserInfoProps> {
  render() {
    const {user} = this.props;
    return (
      <View>
        <UserInfoTop user={user} />
        <UserInfoBottom user={user} />
      </View>
    );
  }
}
