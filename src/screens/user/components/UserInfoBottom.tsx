import {IUser} from '../../../share/interfaces';
import {Text, View} from 'react-native';
import styles from '../styles';
import React from 'react';
import UserRating from './UserRating';

export const UserInfoBottom = ({user}: {user: IUser}) => {
  const {full_name, email, statusLabel, registered_at} = user; // , is_verified
  return (
    <View style={styles.userBottom}>
      <UserParam title={'Рейтинг'} param={<UserRating />} />
      <UserParam title={'Полное имя'} param={full_name} />
      <UserParam title={'Почта'} param={email} />
      <UserParam title={'Статус'} param={statusLabel} />
      <UserParam title={'Зарегистрирован'} param={registered_at} />
    </View>
  );
};

const UserParam = ({
  title,
  param,
}: {
  title: string;
  param: string | React.ReactNode;
}) => {
  return (
    <View style={{flexDirection: 'row', marginTop: 8, alignItems: 'center'}}>
      <Text style={{fontSize: 16}}>{`${title}:`}</Text>
      {typeof param === 'string' ? (
        <Text style={{fontSize: 16, marginLeft: 8}}>{param}</Text>
      ) : (
        param
      )}
    </View>
  );
};
