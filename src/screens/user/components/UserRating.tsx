import React, {Component} from 'react';
import {Text, View} from 'react-native';
import store from '../../../store';
import {observer} from 'mobx-react';
import IconFontAwesome from 'react-native-vector-icons/FontAwesome';
import styles from '../styles';

// user rating with stars component
@observer
export default class UserRating extends Component {
  render() {
    const {reviewsRating} = store.userStore;
    if (!reviewsRating) {
      return null;
    }
    const {rating, count} = reviewsRating;
    return (
      <View style={styles.ratingView}>
        <Icon rating={rating} max={1} />
        <Icon rating={rating} max={2} />
        <Icon rating={rating} max={3} />
        <Icon rating={rating} max={4} />
        <Icon rating={rating} max={5} />
        <Text>{` - ${count} отзыв`}</Text>
      </View>
    );
  }
}

const Icon = ({
  rating,
  max,
}: {
  rating: number;
  max: number;
}) => {
  const half = max - 0.5;
  return (
    <IconFontAwesome
      name={
        rating >= max && rating >= half ? 'star' : rating === half ? 'star-half-o' : 'star-o'
      }
      size={16}
    />
  );
};
