import {IUser} from "../../../share/interfaces";
import {Text, View} from "react-native";
import styles from "../styles";
import React from "react";
import FastImage from "react-native-fast-image";

export const UserInfoTop = ({user}: {user: IUser}) => {
    const {avatarUrl, displayName, activity_type, phone} = user;
    return (
        <View style={styles.userTop}>
            <Avatar uri={avatarUrl} />
            <View style={{justifyContent: 'center', marginLeft: 16}}>
                <Text style={{fontSize: 16}}>{displayName}</Text>
                <Text style={{fontSize: 12, color: 'grey'}}>{activity_type}</Text>
                <Text>{phone}</Text>
            </View>
        </View>
    );
};


const Avatar = ({uri}: {uri: string}) => {
    return (
        <FastImage source={{uri}} style={styles.avatar} resizeMode={'cover'} />
    );
};
