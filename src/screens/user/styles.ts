import {StyleSheet} from 'react-native';
import {BLUE_COLOR} from '../../share/colors';
import stylesGlobal from '../../share/stylesGlobal';

const styles = StyleSheet.create({
  avatar: {
    width: 70,
    height: 70,
    borderRadius: 35,
    borderWidth: 2,
    borderColor: BLUE_COLOR,
  },
  userTop: {
    padding: 16,
    ...stylesGlobal.bottomBorder,
    flexDirection: 'row',
  },
  userBottom: {
    paddingHorizontal: 16,
    paddingBottom: 8,
    ...stylesGlobal.bottomBorder,
  },
  contentList: {
    paddingHorizontal: 16,
  },
  cityListItem: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    borderWidth: 1,
    borderColor: 'grey',
    borderRadius: 8,
    marginHorizontal: 16,
    marginTop: 8,
  },
  cityTextView: {
    flex: 1,
    borderRightWidth: 1,
    borderColor: 'grey',
    padding: 8,
  },
  deleteButton: {
    padding: 8,
  },
  deleteText: {
    fontSize: 16, color: 'red'
  },
  ratingView: {
    flexDirection: 'row', alignItems: 'center', marginLeft: 8
  }
});

export default styles;
