import React, {Component} from 'react';
import {ICity, NavigationProps} from '../../share/interfaces';
import {
  FlatList,
  ListRenderItemInfo,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import stylesGlobal from '../../share/stylesGlobal';
import {observer} from 'mobx-react';
import store from '../../store';
import styles from './styles';
import {ActionButton} from '../../share/components/ActionButton';
import UserInfo from './components/UserInfo';
import {toJS} from 'mobx';

@observer
export default class UserScreen extends Component<NavigationProps> {
  componentDidMount() {
    const {params = {}} = this.props.navigation.state;
    const {isFromCode} = params;
    console.log({isFromCode});
    // checking navigation from CodeScreen to not send a second request
    if (!isFromCode) {
      store.userStore.initUserInfo();
    }
  }

  //#region flatList props
  keyExtractor = (item: ICity) => item.id.toString();

  renderItem = ({item: city, index}: ListRenderItemInfo<ICity>) => {
    return (
      <View style={styles.cityListItem}>
        <View style={styles.cityTextView}>
          <Text style={{fontSize: 16}}>{city.name}</Text>
        </View>
        <TouchableOpacity
          onPress={() => store.userStore.deleteCity(index)}
          style={styles.deleteButton}>
          <Text style={styles.deleteText}>УДАЛИТЬ</Text>
        </TouchableOpacity>
      </View>
    );
  };
  //#endregion

  onPressAddCity = () => {
    this.props.navigation.navigate('CitiesScreen');
  };

  render() {
    const {user, userCities, isLoading, getUserInfo} = store.userStore;
    if (!user) {
      return null;
    }
    console.log({user: toJS(user), userCities: toJS(userCities)});
    return (
      <View style={stylesGlobal.container}>
        <FlatList
          ListHeaderComponent={<UserInfo user={user} />}
          keyExtractor={this.keyExtractor}
          data={userCities.slice()}
          refreshing={isLoading}
          onRefresh={getUserInfo}
          renderItem={this.renderItem}
        />
        <ActionButton
          style={{margin: 8}}
          onPress={this.onPressAddCity}
          text={'ДОБАВИТЬ'}
        />
      </View>
    );
  }
}
