import React, {Component} from 'react';
import {TextInput, View} from 'react-native';
import styles from '../styles';
import store from '../../../store';
import Icon from 'react-native-vector-icons/MaterialIcons';
import {observer} from "mobx-react";

@observer
export default class SearchInput extends Component {
  render() {
    return (
      <View style={styles.searchView}>
        <Icon style={styles.iconSearch} name={'search'} />
        <TextInput
          placeholder={'Поиск'}
          style={{flex: 1, fontSize: 18}}
          value={store.geoStore.searchText}
          onChangeText={store.geoStore.setSearchText}
        />
      </View>
    );
  }
}
