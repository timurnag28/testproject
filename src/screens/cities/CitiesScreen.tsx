import React, {Component} from 'react';
import {observer} from 'mobx-react';
import store from '../../store';
import {
  FlatList,
  ListRenderItemInfo,
  TouchableOpacity,
  View,
  Text,
} from 'react-native';
import {ICity, NavigationProps} from '../../share/interfaces';
import stylesGlobal from '../../share/stylesGlobal';
import styles from './styles';
import SearchInput from './components/SearchInput';

@observer
export default class CitiesScreen extends Component<NavigationProps> {
  componentDidMount() {
    // after did mount get cities
    store.geoStore.getCities();
  }

  onPressCity = (city: ICity) => {
    this.props.navigation.goBack();
    store.userStore.addCity(city);
  };

  ///#region flatList props
  renderItem = ({item}: ListRenderItemInfo<ICity>) => {
    return (
      <TouchableOpacity
        onPress={() => this.onPressCity(item)}
        style={styles.listItem}>
        <Text style={styles.cityText}>{item.name}</Text>
      </TouchableOpacity>
    );
  };

  keyExtractor = (city: ICity) => city.id.toString();
  //#endregion

  render() {
    const {cities, isLoading, getCities} = store.geoStore;
    return (
      <View style={stylesGlobal.container}>
        <SearchInput />
        <FlatList
          refreshing={isLoading}
          onRefresh={getCities}
          ListEmptyComponent={
            !isLoading ? (
              <Text style={styles.noItemsText}>Не удалось получить данные</Text>
            ) : null
          }
          style={styles.list}
          keyExtractor={this.keyExtractor}
          data={cities.slice()}
          renderItem={this.renderItem}
        />
      </View>
    );
  }
}
