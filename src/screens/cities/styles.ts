import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  list: {
    paddingHorizontal: 16,
  },
  listItem: {
    paddingVertical: 16,
  },
  cityText: {
    fontSize: 16,
  },
  noItemsText: {
    fontSize: 16,
    textAlign: 'center',
    marginTop: 16,
  },
  searchView: {
    borderRadius: 4,
    borderWidth: 1,
    borderColor: 'grey',
    flexDirection: 'row',
    alignItems: 'center',
    height: 50,
    margin: 16,
    overflow: 'hidden',
  },
  iconSearch: {
    fontSize: 20,
    color: 'grey',
    paddingHorizontal: 8,
  },
});
export default styles;
