import React, {Component} from 'react';
import {NavigationProps} from '../../share/interfaces';
import {View} from 'react-native';
import styles from './styles';
import {InputView} from './components/InputView';
import {ActionButton} from '../../share/components/ActionButton';
import {observer} from 'mobx-react';
import store from '../../store';

@observer
export default class CodeScreen extends Component<NavigationProps> {
  onPressConfirm = () => {
    store.authStore
      .confirm()
      .then(() => {
        this.props.navigation.navigate('UserScreen', {isFromCode: true});
      })
      .catch(() => null);
  };

  render() {
    const {code, setCode} = store.authStore;
    return (
      <View style={styles.container}>
        <View style={styles.form}>
          <InputView
            maxLength={4}
            placeholder={'Код из смс'}
            value={code}
            onChangeText={setCode}
          />
          <ActionButton
            style={{marginTop: 16}}
            text={'ПОДТВЕРДИТЬ'}
            onPress={this.onPressConfirm}
          />
        </View>
      </View>
    );
  }
}
