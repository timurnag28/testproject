import React, {Component} from 'react';
import {NavigationProps} from '../../share/interfaces';
import {View} from 'react-native';
import {observer} from 'mobx-react';
import store from '../../store';
import styles from './styles';
import {InputView} from './components/InputView';
import {ActionButton} from '../../share/components/ActionButton';

@observer
export default class LoginScreen extends Component<NavigationProps> {
  onPressLogin = () => {
    store.authStore
      .auth()
      .then(() => {
        this.props.navigation.navigate('CodeScreen');
      })
      .catch(() => null);
  };

  render() {
    const {phone, setPhone} = store.authStore;
    return (
      <View style={styles.container}>
        <View style={styles.form}>
          <InputView
            maxLength={12}
            placeholder={'Номер телефона'}
            value={phone}
            onChangeText={setPhone}
          />
          <ActionButton
            style={{marginTop: 16}}
            text={'Войти'}
            onPress={this.onPressLogin}
          />
        </View>
      </View>
    );
  }
}
