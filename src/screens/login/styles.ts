import {StyleSheet} from 'react-native';
import {BLUE_COLOR} from '../../share/colors';
import stylesGlobal from '../../share/stylesGlobal';

const styles = StyleSheet.create({
  container: {
    ...stylesGlobal.container,
    justifyContent: 'center',
  },
  form: {
    backgroundColor: '#fff',
    padding: 16,
    marginHorizontal: 16,
  },
  inputView: {
    borderRadius: 8,
    borderWidth: 1,
    borderColor: BLUE_COLOR,
  },
  input: {
    fontSize: 16,
  },
});
export default styles;
