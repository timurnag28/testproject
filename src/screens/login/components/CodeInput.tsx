import React, {Component} from 'react';
import {View} from 'react-native';
import {InputView} from './InputView';
import {observer} from 'mobx-react';
import store from '../../../store';

@observer
export default class CodeInput extends Component {
  onChangeCode = (text: string, index: number) => {
    let {code} = store.authStore;
    code = code.substr(0, index) + text[0] + code.substr(index, code.length);
    console.log({code});
    store.authStore.setCode(
      code.substr(0, index) + text[0] + code.substr(index, code.length),
    );
  };

  render() {
    const {code} = store.authStore;
    return (
      <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
        <InputCode code={code} index={0} onChangeCode={this.onChangeCode} />
        <InputCode code={code} index={1} onChangeCode={this.onChangeCode} />
        <InputCode code={code} index={2} onChangeCode={this.onChangeCode} />
        <InputCode code={code} index={3} onChangeCode={this.onChangeCode} />
      </View>
    );
  }
}

const InputCode = ({
  code,
  index,
  onChangeCode,
}: {
  code: string;
  index: number;
  onChangeCode: (text: string, index: number) => void;
}) => {
  return (
    <InputView
      style={{textAlign: 'center'}}
      value={code[index] ? code[index] : ''}
      onChangeText={text => onChangeCode(text, index)}
      placeholder={'0'}
      maxLength={1}
    />
  );
};
