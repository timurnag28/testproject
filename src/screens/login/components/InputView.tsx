import {StyleProp, TextInput, TextStyle, View} from 'react-native';
import styles from '../styles';
import React from 'react';

export const InputView = ({
  value,
  onChangeText,
  placeholder,
  maxLength,
  style,
}: {
  value: string;
  onChangeText: (text: string) => void;
  placeholder: string;
  maxLength: number;
  style?: StyleProp<TextStyle>;
}) => {
  return (
    <View style={styles.inputView}>
      <TextInput
        maxLength={maxLength}
        style={[styles.input, style]}
        keyboardType={'phone-pad'}
        placeholder={placeholder}
        value={value}
        onChangeText={onChangeText}
      />
    </View>
  );
};
