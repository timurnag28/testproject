import {BASE_URL} from './index';

// get cities
export function getCities(): Promise<Response> {
  const token = ''; // TODO async storage
  return fetch(`${BASE_URL}/geo/index`, {
    method: 'GET',
    headers: {
      'Access-token': token, // api doesn't use token -_-
    },
  });
}
