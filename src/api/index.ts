import {IConfirmCodeBody, IUserUpdateBody} from '../share/interfaces';
import {auth, confirmCode} from './auth-api';
import {getUserInfo, updateUser} from './user-api';
import {getCities} from './api-geo';

export const BASE_URL = 'https://disinfect-dev.netimob.com/api';

export default class API {
  // Auth API
  static auth = {
    auth: (phone: string) => interceptor(auth(phone)),
    confirmCode: (codeBody: IConfirmCodeBody) =>
      interceptor(confirmCode(codeBody)),
  };
  // User API
  static user = {
    getUserInfo: () => interceptor(getUserInfo()),
    updateUser: (userBody: IUserUpdateBody) =>
      interceptor(updateUser(userBody)),
  };
  // Geo API
  static geo = {
    getCities: () => interceptor(getCities()),
  };
}

// interceptor(common function for api requests)
export const interceptor = (method: Promise<Response>) => {
  return method
    .then(async (response: Response) => {
      console.log({response});
      switch (response.status) {
        case 200:
          return await response.json();
        case 404: {
          throw new Error('Не найдено');
        }
        default: {
          throw new Error(response.status.toString());
        }
      }
    })
    .catch((error: Error) => {
      if (error.message === 'Network request failed') {
        error.message = 'Ошибка соединения';
      }
      throw error;
    });
};
