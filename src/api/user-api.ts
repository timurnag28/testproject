import {BASE_URL} from './index';
import {IUserUpdateBody} from '../share/interfaces';
import store from '../store';

// get user info
export async function getUserInfo(): Promise<Response> {
  const token = await store.authStore.getLocalToken();
  return fetch(`${BASE_URL}/user/info`, {
    method: 'GET',
    headers: {
      'Access-token': token,
    },
  });
}

// update cities
export async function updateUser(userBody: IUserUpdateBody): Promise<Response> {
  const token = await store.authStore.getLocalToken();
  return fetch(`${BASE_URL}/user/update`, {
    method: 'POST',
    headers: {
      'Access-token': token,
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(userBody),
  });
}
