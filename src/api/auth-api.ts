import {BASE_URL} from './index';
import {IConfirmCodeBody} from "../share/interfaces";

// auth
export function auth(phone: string): Promise<Response> {
  return fetch(`${BASE_URL}/auth/auth`, {
    method: 'POST',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({phone}),
  });
}

// confirmCode sms
export function confirmCode(codeBody: IConfirmCodeBody): Promise<Response> {
  return fetch(`${BASE_URL}/auth/confirm`, {
    method: 'POST',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(codeBody),
  });
}
