import {createStackNavigator} from 'react-navigation-stack';
import LoginScreen from '../screens/login/LoginScreen';
import CodeScreen from '../screens/login/CodeScreen';

// auth stack
export default createStackNavigator(
  {
    LoginScreen: {
      screen: LoginScreen,
    },
    CodeScreen: {
      screen: CodeScreen,
    },
  },
  {
    initialRouteName: 'LoginScreen',
    defaultNavigationOptions: {
      header: () => null,
      cardStyle: {
        backgroundColor: '#cacaca',
      },
    },
  },
);
