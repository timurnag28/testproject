import {createStackNavigator} from 'react-navigation-stack';
import UserScreen from '../screens/user/UserScreen';
import CitiesScreen from '../screens/cities/CitiesScreen';

// MainStack
export default createStackNavigator(
  {
    UserScreen: {
      screen: UserScreen,
      navigationOptions: {
        headerTitle: 'Пользователь'
      }
    },
    CitiesScreen: {
      screen: CitiesScreen,
      navigationOptions: {
        headerTitle: 'Добавление города'
      }
    },
  },
  {
    initialRouteName: 'UserScreen',
    defaultNavigationOptions: {
      cardStyle: {
        backgroundColor: '#fff',
      },
    },
  },
);
