import {createAppContainer, createSwitchNavigator} from 'react-navigation';
import InitScreen from './InitScreen';
import LoginStack from './LoginStack';
import MainStack from './MainStack';

const AppNavigator = createSwitchNavigator(
  {
    initRoute: InitScreen,
    loginStack: LoginStack,
    mainStack: MainStack,
  },
  {
    // initialRouteParams: {},
    initialRouteName: 'initRoute',
  },
);
const AppContainer = createAppContainer(AppNavigator);
export default AppContainer;
