import React from 'react';
import store from '../store';
import { NavigationProps } from '../share/interfaces';

// screen to check local token
export default class InitScreen extends React.Component<NavigationProps> {
  componentDidMount() {
    store.authStore.getLocalToken().then((token) => {
      if (token) {
        this.props.navigation.navigate('mainStack');
      } else {
        this.props.navigation.navigate('loginStack');
      }
    });
  }

  render() {
    return null;
  }
}
